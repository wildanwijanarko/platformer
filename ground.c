// Copyright (c) 2023 Wildan R Wijanarko (@wildan9)
//
// This software is provided ‘as-is’, without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.

// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:

// 1. The origin of this software must not be misrepresented; you must not
// claim that you wrote the original software. If you use this software
// in a product, an acknowledgment in the product documentation would be
// appreciated but is not required.

// 2. Altered source versions must be plainly marked as such, and must not be
// misrepresented as being the original software.

// 3. This notice may not be removed or altered from any source
// distribution.

#include "ground.h"

bool drawVertex = true;

void InitGround()
{
    float screenWidth = GetScreenWidth();
    float screenHeight = GetScreenHeight();

    // Create floor and walls rectangle physics body
    PhysicsBody floor0 = CreatePhysicsBodyRectangle((Vector2){ screenWidth / 2.0f, (float)screenHeight }, (float)screenWidth, 100, 10);
    PhysicsBody floor1 = CreatePhysicsBodyRectangle((Vector2){ screenWidth / 2.0f * 2 + 600.0f, (float)screenHeight }, (float)screenWidth, 100, 10);
    PhysicsBody floor2 = CreatePhysicsBodyRectangle((Vector2){ screenWidth / 2.0f * 3 + 600.0f * 2, (float)screenHeight }, (float)screenWidth, 100, 10);
    PhysicsBody floor3 = CreatePhysicsBodyRectangle((Vector2){ screenWidth / 2.0f * 4 + 600.0f * 3, (float)screenHeight }, (float)screenWidth, 100, 10);
    PhysicsBody floor4 = CreatePhysicsBodyRectangle((Vector2){ screenWidth / 2.0f * 5 + 600.0f * 4, (float)screenHeight }, (float)screenWidth, 100, 10);
    PhysicsBody floor5 = CreatePhysicsBodyRectangle((Vector2){ screenWidth / 2.0f * 6 + 600.0f * 5, (float)screenHeight }, (float)screenWidth, 100, 10);

    PhysicsBody ground0 = CreatePhysicsBodyRectangle((Vector2){ screenWidth * 0.25f,              screenHeight * 0.6f }, screenWidth * 0.25f, 10, 10);
    PhysicsBody ground1 = CreatePhysicsBodyRectangle((Vector2){ screenWidth * 0.25f + 600.0f,     screenHeight * 0.5f }, screenWidth * 0.25f, 10, 10);
    PhysicsBody ground2 = CreatePhysicsBodyRectangle((Vector2){ screenWidth * 0.25f + 600.0f * 3, screenHeight * 0.3f }, screenWidth * 0.25f, 10, 10);
    PhysicsBody ground3 = CreatePhysicsBodyRectangle((Vector2){ screenWidth * 0.25f + 600.0f * 4, screenHeight * 0.6f }, screenWidth * 0.25f, 10, 10);
    PhysicsBody ground4 = CreatePhysicsBodyRectangle((Vector2){ screenWidth * 0.25f + 600.0f * 5, screenHeight * 0.6f }, screenWidth * 0.25f, 10, 10);

    // Disable dynamics to floor and walls physics bodies
    floor0->enabled = false;
    floor1->enabled = false;
    floor2->enabled = false;
    floor3->enabled = false;
    floor4->enabled = false;
    floor5->enabled = false;

    ground0->enabled = false;
    ground1->enabled = false;
    ground2->enabled = false;
    ground3->enabled = false;
    ground4->enabled = false;
}

void DrawGroundVertex()
{
    if (IsKeyPressed(KEY_G)) drawVertex = !drawVertex;
    
    if (drawVertex)
    {
        // Draw created physics bodies
        int bodiesCount = GetPhysicsBodiesCount();
        for (int i = 0; i < bodiesCount; i++)
        {
            PhysicsBody body = GetPhysicsBody(i);

            int vertexCount = GetPhysicsShapeVerticesCount(i);
            for (int j = 0; j < vertexCount; j++)
            {
                // Get physics bodies shape vertices to draw lines
                // Note: GetPhysicsShapeVertex() already calculates rotation transformations
                Vector2 vertexA = GetPhysicsShapeVertex(body, j);

                int jj = (((j + 1) < vertexCount) ? (j + 1) : 0);   // Get next vertex or first to close the shape
                Vector2 vertexB = GetPhysicsShapeVertex(body, jj);

                DrawLineV(vertexA, vertexB, GREEN);     // Draw a line between two vertex positions
            }
        }
    }
}